import datetime
from dataclasses import dataclass, field

from marshmallow import Schema, fields, post_load

from typing import List, Optional, Dict

from worg.availability import AvailabilityCheckerSchema, AvailabilityChecker, AlwaysAvailableAvailabilityChecker


class ProfileSchema(Schema):
    name = fields.Str(required=True)
    interval = fields.TimeDelta(precision='days', required=True)
    env = fields.Dict(keys=fields.Str(), values=fields.Str(), required=False)
    availability = fields.Nested(AvailabilityCheckerSchema, required=False)
    script = fields.List(fields.Str(), required=True)
    variables = fields.Dict(keys=fields.Str(), values=fields.Str(), required=False, missing={})

    @post_load
    def make_profile(self, data, **kwargs):
        return Profile(**data)


@dataclass
class Profile:
    name: str
    interval: datetime.timedelta
    script: List[str]
    availability: AvailabilityChecker = AlwaysAvailableAvailabilityChecker()
    env: Optional[Dict[str, str]] = field(default_factory=lambda: {})
    variables: Optional[Dict[str, str]] = field(default_factory=lambda: {})
