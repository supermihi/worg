import datetime
import dateutil.parser
import os
import psutil
import subprocess
from pathlib import Path
from worg.config import Configuration
from worg.profiles import Profile


class ProfileRunner:

    def __init__(self, profile: Profile, config: Configuration):
        self.profile = profile
        self.config = config

    @property
    def pidfile(self):
        return Path(self.config.worg_config_dir) / f'worg_{self.profile.name}.pid'

    def create_pidfile(self):
        pid = str(os.getpid())
        self.pidfile.write_text(pid)

    def remove_pidfile(self):
        if self.pidfile.exists():
            self.pidfile.unlink()

    def pid_of_running_worg(self):
        pidfile = self.pidfile
        if pidfile.exists():
            pid = int(self.pidfile.read_text())
            if psutil.pid_exists(pid):
                return pid
            else:
                pidfile.unlink()

    @property
    def last_completed_file(self):
        return self.config.worg_config_dir / f'last_complete_{self.profile.name}'

    def last_completed(self):
        last_completed_file = self.last_completed_file
        if last_completed_file.exists():
            content = last_completed_file.read_text()
            return dateutil.parser.parse(content.strip())
        return datetime.datetime.min

    def store_last_completed(self):
        now = datetime.datetime.now()
        last = now.isoformat()
        self.last_completed_file.write_text(last)

    def is_due(self):
        now = datetime.datetime.now()
        last = self.last_completed()
        return now - last > self.profile.interval

    def can_run(self):
        return self.profile.availability is not None and self.profile.availability.is_available()

    def run(self):
        pid = self.pid_of_running_worg()
        if pid is not None:
            raise RuntimeError('Profile {} already running with PID {}'.format(self.profile.name, pid))
        self.profile.availability.assertis_available()
        self.create_pidfile()
        try:
            self.try_run_script()
            self.store_last_completed()
        except subprocess.CalledProcessError as e:
            print(f'error: {e}')
        finally:
            self.remove_pidfile()

    def try_run_script(self):
        print(f'Running backup profile "{self.profile.name}" ...')
        for command in self.profile.script:
            subprocess.run(command.format(**self.profile.variables), shell=True, check=True, env=self.get_env())
        print('Backup finished. Please unplug device!')

    def get_env(self):
        os_env = dict(os.environ)
        os_env.update(self.profile.env)
        return os_env

    def __str__(self):
        return f'ProfileRunner({self.profile.name})'

    def is_running(self):
        pid = self.pid_of_running_worg()
        return pid is not None
