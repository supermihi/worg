import argparse
import json
from typing import Dict

from worg.config import Configuration
from worg.profiles import ProfileSchema, Profile
from worg.runner import ProfileRunner


def create_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='command help', dest='command')
    parser_list = subparsers.add_parser('list')
    parser_run = subparsers.add_parser('run')
    parser_run.add_argument('profile')
    parser_check = subparsers.add_parser('check')
    return parser


def run_script():
    config = Configuration.default()
    profiles = load_profiles(config)

    parser = create_parser()
    args = parser.parse_args()
    if args.command == 'list':
        list(profiles)
    elif args.command == 'run':
        profile = profiles[args.profile]
        run(profile, config)
    elif args.command == 'check' or not args.command:
        check(profiles, config)
    else:
        raise NotImplementedError()


def run(profile: Profile, config: Configuration):
    ProfileRunner(profile, config).run()


def list(profiles: Dict[str, Profile]):
    for profile in profiles:
        print(profile)


def check(profiles: Dict[str, Profile], config: Configuration):
    runners = [ProfileRunner(p, config) for p in profiles.values()]

    running = [p for p in runners if p.is_running()]
    due_not_running = [p for p in runners if p.is_due() and p not in running]
    if len(due_not_running) == 0:
        if len(running) == 0:
            print('all backups good')
        else:
            print(f'all backups good ({", ".join(r.profile.name for r in running)} running)')
        return
    for profile in due_not_running:
        input(f'"{profile.profile.name}" is due! (Last completion time is {profile.last_completed()})')
        if profile.can_run():
            ans = input('run now? [Y/n] ')
            if ans in "YyjJ":
                profile.run()


def load_profiles(config) -> Dict[str, Profile]:
    schema = ProfileSchema(many=True)
    json_profiles = json.loads(config.profiles_path.read_text())
    profiles = schema.load(json_profiles)
    return {p.name: p for p in profiles}


if __name__ == '__main__':
    run_script()
