from dataclasses import dataclass
from pathlib import Path


@dataclass
class Configuration:
    worg_config_dir: Path

    @property
    def profiles_path(self):
        return self.worg_config_dir / 'profiles.json'
    @staticmethod
    def default():
        return Configuration(worg_config_dir=get_default_worg_config_dir())


def get_default_worg_config_dir():
    import xdg.BaseDirectory
    return Path(xdg.BaseDirectory.xdg_config_home) / 'worg'

