import os.path
import subprocess
from marshmallow import Schema, fields, INCLUDE, post_load


class BackupNotAvailableException(RuntimeError):
    pass


class AvailabilityCheckerSchema(Schema):
    type = fields.Str(required=True)

    class Meta:
        unknown = INCLUDE

    @post_load
    def make_checker(self, data, **kwargs):
        if data['type'] == 'device':
            return DeviceAvailabilityCheckerSchema().load(data)


class DeviceAvailabilityCheckerSchema(AvailabilityCheckerSchema):
    path = fields.Str(required=True)

    @post_load
    def make_checker(self, data, **kwargs):
        return DeviceAvailabilityChecker(data['path'])


class AvailabilityChecker:

    def is_available(self):
        raise NotImplementedError()

    def message(self):
        raise NotImplementedError()

    def assertis_available(self):
        if not self.is_available():
            raise BackupNotAvailableException(self.message())


class DeviceAvailabilityChecker(AvailabilityChecker):

    def __init__(self, path):
        self.path = path

    def is_available(self):
        return self.path and os.path.exists(self.path)

    def message(self):
        return f"Device {self.path} not available"


class PingAvailabilityChecker(AvailabilityChecker):

    def __init__(self, url):
        self.url = url

    def is_available(self):
        if self.url is None:
            return False
        return subprocess.call(['ping', '-c1', '-q', '-W1', self.url], stdout=subprocess.DEVNULL,
                               stderr=subprocess.DEVNULL)

    def message(self):
        return f'URL {self.url} not available'


class AlwaysAvailableAvailabilityChecker(AvailabilityChecker):

    def is_available(self):
        return True

    def message(self):
        return "Should not see this!"
