from datetime import timedelta

from worg.config import Configuration
from worg.profiles import Profile
from worg.runner import ProfileRunner


def test_runner_executes(tmp_path):
    profile = Profile('test', timedelta(days=14), [f'touch {str(tmp_path / "hallo")}'])
    config = Configuration(tmp_path)
    runner = ProfileRunner(profile, config)
    runner.run()
    assert (tmp_path / 'hallo').exists()
