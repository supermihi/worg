import datetime

from worg.availability import DeviceAvailabilityChecker
from worg.profiles import ProfileSchema, Profile


def test_deserialize_profile():
    schema_data = {'name': 'test_profile', 'script': ['rm -rf', 'ls'], 'interval': 14, }
    schema = ProfileSchema().load(schema_data)

    assert isinstance(schema, Profile)
    assert schema.name == 'test_profile'
    assert schema.interval == datetime.timedelta(days=14)

def test_deserialize_profile_with_device_availability_checker():
    schema_data = {'name': 'test_profile', 'script': ['rm -rf', 'ls'], 'interval': 14,
                   'availability': { 'type': 'device', 'path': '/a/b/c'}}
    schema = ProfileSchema().load(schema_data)

    assert isinstance(schema, Profile)
    assert schema.name == 'test_profile'
    assert isinstance(schema.availability, DeviceAvailabilityChecker)
