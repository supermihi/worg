from pathlib import Path
from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name='worg',
    version='0.0.1',
    packages=['worg'],
    url='https://gitlab.com/supermihi/worg',
    license='GPLv3',
    author='Michael Helmling',
    author_email='michaelhelmling@posteo.de',
    description='simple backup reminder script',
    entry_points={'console_scripts': ['worg = worg.script:run_script']},
)
