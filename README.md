# worg

Backup reminder

# Configuration

Create `~/.config/work/profiles.json`. Example:

```json
[
	{
		"name": "fullbackup",
		"interval": 14, // days
		"availability": {
			"type": "device",
			"path": "/dev/disk/by-label/my_backup_hdd"
		},
		"env": {
			"BORG_PASSPHRASE": "XYZ"
		},
		"script": [
			"borg create --stats -p /run/media/my_backup_hdd/fullbackup/repository::fullbackup-$(date +%Y-%m-%d) /home/me /data --exclude-from ~/.config/borg_excludes",
			"borg prune -P fullbackup -s --list -w 3 -m 3 -y1 /run/media/my_backup_hdd/fullbackup
		]
	}
]
```